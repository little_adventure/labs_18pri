package src.main.java.com.example.ch07.tsk07;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Write a program that reads all words in a file and prints out how often
 * each word occurred. Use a TreeMap<String, Integer>.
 */

public class Program {
     private final static String FILE_NAME = "/home/skeptlk/Downloads/Пелевин. Transhumanism Inc.fb2";
//    private final static String FILE_NAME = "/home/skeptlk/Downloads/dict.txt";

    private static final Logger logger = Logger.getLogger(Program.class.getName());

    public static void main(String[] args) {
        try {

            Scanner sc = new Scanner(new File(FILE_NAME));
            TreeMap<String, Integer> map = getWordsStat(sc);
            print10Greatest(map);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static TreeMap<String, Integer> getWordsStat (Scanner sc) {
        TreeMap<String, Integer> map = new TreeMap<>();

        while (sc.hasNextLine()) {
            Scanner s2 = new Scanner(sc.nextLine());
            while (s2.hasNext()) {
                String s = s2.next().toLowerCase(Locale.ROOT);
                map.merge(s, 1, Integer::sum);
            }
        }

        return map;
    }

    private static <K, V extends Comparable<V> > void print10Greatest(final Map<K, V> map) {

        Stream<Map.Entry<K, V>> stream = map
                .entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()));
        AtomicInteger c = new AtomicInteger(0);

        stream.takeWhile(k -> c.incrementAndGet() <= 10)
            .forEach(kvEntry ->
                logger.log(Level.INFO, kvEntry.getKey() + " => " + kvEntry.getValue())
            );
    }

}
