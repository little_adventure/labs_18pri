package src.main.java.com.example.ch07.tsk09;

import org.junit.Test;

import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

/**
 * You can update the counter in a map of counters as
 * counts.merge(word, 1, Integer::sum);
 * Do the same without the merge method,
 * (a) by using contains,
 * (b) by using get and a null check,
 * (c) by using getOrDefault,
 * (d) by using putIfAbsent.
 */

public class CountersTest {
    @Test
    public void merge () {
        TreeMap<String, Integer> map = new TreeMap<>();

        map.merge("Hello", 1, Integer::sum);
        map.merge("Hello", 1, Integer::sum);
        map.merge("Hello", 1, Integer::sum);

        assertEquals(3, (int) map.get("Hello"));
    }

    @Test
    public void contains () {
        TreeMap<String, Integer> map = new TreeMap<>();

        if (!map.containsKey("Hello")) {
            map.put("Hello", 1);
        } else {
            map.put("Hello", map.get("Hello") + 1);
        }

        if (!map.containsKey("Hello")) {
            map.put("Hello", 1);
        } else {
            map.put("Hello", map.get("Hello") + 1);
        }

        if (!map.containsKey("Hello")) {
            map.put("Hello", 1);
        } else {
            map.put("Hello", map.get("Hello") + 1);
        }

        assertEquals(3, (int) map.get("Hello"));
    }

    @Test
    public void nullCompare () {
        TreeMap<String, Integer> map = new TreeMap<>();

         if (map.get("Hello") == null) map.put("Hello", 1);
         else map.put("Hello", map.get("Hello") + 1);

        if (map.get("Hello") == null) map.put("Hello", 1);
        else map.put("Hello", map.get("Hello") + 1);

        if (map.get("Hello") == null) map.put("Hello", 1);
        else map.put("Hello", map.get("Hello") + 1);

        assertEquals(3, (int) map.get("Hello"));
    }

    @Test
    public void getOrDefault () {
        TreeMap<String, Integer> map = new TreeMap<>();

        map.put("Hello", map.getOrDefault("Hello", 0) + 1);
        map.put("Hello", map.getOrDefault("Hello", 0) + 1);
        map.put("Hello", map.getOrDefault("Hello", 0) + 1);

        assertEquals(3, (int) map.get("Hello"));
    }

    @Test
    public void putIfAbsent () {
        TreeMap<String, Integer> map = new TreeMap<>();

        map.putIfAbsent("Hello", 0);
        map.put("Hello", map.get("Hello") + 1);
        map.putIfAbsent("Hello", 0);
        map.put("Hello", map.get("Hello") + 1);
        map.putIfAbsent("Hello", 0);
        map.put("Hello", map.get("Hello") + 1);

        assertEquals(3, (int) map.get("Hello"));
    }
}


