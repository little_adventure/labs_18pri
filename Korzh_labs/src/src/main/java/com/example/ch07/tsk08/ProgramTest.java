package src.main.java.com.example.ch07.tsk08;
import org.junit.Test;

import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeMap;
import static org.junit.Assert.*;


public class ProgramTest {
    @Test
    public void getOccurrences() {
        String data = """
                a b c
                abc ... 123 abc
                abc a a a
        """;

        TreeMap<String, HashSet<Integer>> res = Program.getOccurrences(new Scanner(data));

        HashSet<Integer> o1 = res.get("a");
        assertEquals(4, o1.size());

        HashSet<Integer> o2 = res.get("...");
        assertEquals(1, o2.size());
        assertTrue(o1.contains(1));
    }
}
