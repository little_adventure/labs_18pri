package src.main.java.com.example.ch07.tsk07;

import org.junit.Test;
import java.util.Scanner;
import java.util.TreeMap;
import static org.junit.Assert.*;

public class ProgramTest {
    @Test
    public void getWordsStat() {

        String data = """
                a b c
                abc ... 123 abc
                abc a a a
        """;

        TreeMap<String, Integer> stat = Program.getWordsStat(new Scanner(data));

        assertEquals(4, (int) stat.get("a"));
        assertEquals(1, (int) stat.get("b"));
        assertEquals(1, (int) stat.get("c"));
        assertEquals(3, (int) stat.get("abc"));
        assertNull(stat.get("___"));
    }
}
