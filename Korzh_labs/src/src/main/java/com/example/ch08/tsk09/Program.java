package src.main.java.com.example.ch08.tsk09;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/**
 * Read the words from /usr/share/dict/words (or a similar word list)
 * into a stream and produce an array of all words containing five distinct vowels.
 */

public class Program {
    public static String[] getWordsWithNVowels (Scanner scanner, int vowelsN) {
        final Character[] vowels = new Character[] {'a', 'e', 'i', 'o', 'u', 'y'};
        return scanner.tokens().filter(word -> {
            String wordl = word.toLowerCase();
            int count = 0, shift = 0;
            for (Character vowel : vowels) {
                shift = 0;
                while (0 != (shift = wordl.indexOf(vowel, shift) + 1))
                    count++;
            }
            return count == vowelsN;
        }).toArray(String[]::new);
    }

    public static void main(String[] args) throws FileNotFoundException {
        Instant start = Instant.now();

        File dict = new File("/usr/share/dict/words");
        String[] words = getWordsWithNVowels(new Scanner(dict), 5);

        for (String word : words) {
            System.out.println(word);
        }

        Instant end = Instant.now();
        System.out.println("Time taken: " + Duration.between(start, end));
    }
}
