package src.main.java.com.example.ch08.tsk07;
import org.junit.Test;

import java.util.Scanner;
import java.util.stream.Stream;

import static org.junit.Assert.*;


public class ProgramTest {
    @Test
    public void getOccurrences() {
        String data = """
                a b c
                abc ... 123 abc
                abc a a a
        """;

        Stream<String> words = Program.getWords(new Scanner(data));
        Object[] wordArray = words.toArray();

        assertArrayEquals(
            new String[] {"a", "b", "c", "abc", "abc", "abc", "a", "a", "a"},
            wordArray
        );

        assertEquals(9, wordArray.length);
    }
}
