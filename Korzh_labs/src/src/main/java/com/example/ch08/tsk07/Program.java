package src.main.java.com.example.ch08.tsk07;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Turning a file into a stream of tokens, list the first 100 tokens that are words in the
 * sense of the preceding exercise. Read the file again and list the 10 most frequent
 * words, ignoring letter case.
 */

public class Program {
    private final static String FILE_NAME = "/home/skeptlk/Downloads/dict.txt";
    private static final Logger logger = Logger.getLogger(src.main.java.com.example.ch07.tsk07.Program.class.getName());

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(new File(FILE_NAME));

            AtomicInteger count = new AtomicInteger();
            getWords(sc)
                .takeWhile(s -> count.getAndIncrement() <= 10)
                .forEach(System.out::println);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Stream<String> getWords(Scanner file) {
        return file.tokens().filter(s -> {
            for (int i : s.codePoints().toArray()) {
                if (!Character.isAlphabetic(i))
                    return false;
            }
            return true;
        });
    }
}
