package src.main.java.com.example.ch02.tsk05;

public class Point {
    private double x, y;

    public double getX() {return x;}

    public double getY() {return y;}

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        x = p.x; y = p.y;
    }

    public Point translate(double deltaX, double deltaY) {
        Point p = new Point(
                x + deltaX,
                y + deltaY
        );
        return p;
    }

    /**
     * Scale point by factor
     * @param factor
     * @return
     */
    public Point scale(double factor) {
        Point p = new Point(
                x * factor,
                y * factor
        );
        return p;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
