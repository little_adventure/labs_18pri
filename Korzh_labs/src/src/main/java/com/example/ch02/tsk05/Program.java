package src.main.java.com.example.ch02.tsk05;

/**
 * Implement an immutable class Point that describes a point in the plane. Provide
 * a constructor to set it to a specific point, a no-arg constructor to set it to the origin,
 * and methods getX, getY, translate, and scale. The translate method
 * moves the point by a given amount in x- and y-direction. The scale method
 * scales both coordinates by a given factor. Implement these methods so that they
 * return new points with the results.
 */

/**
 * Repeat the preceding exercise, but now make translate and scale into
 * mutators.
 * Add javadoc comments to both versions of the Point class from the
 * preceding exercises.
 */

public class Program {
    public static void main(String[] args) {
        Point p = new Point(3, 4).translate(1, 3).scale(0.5);
        MutablePoint p2 = new MutablePoint(3, 4).translate(1, 3).scale(0.5);

        System.out.println(p);
        System.out.println(p2);
    }

}
