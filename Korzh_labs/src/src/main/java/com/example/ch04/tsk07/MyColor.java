package src.main.java.com.example.ch04.tsk07;

public enum MyColor {
    BLACK(0, 0, 0),
    RED(255, 0, 0),
    BLUE(0, 0, 255),
    GREEN(0, 255, 0),
    CYAN(0,0,0),
    MAGENTA(0,0,0),
    YELLOW(0,0,0),
    WHITE(255, 255, 255);

    int red, green, blue;

    MyColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }
}
