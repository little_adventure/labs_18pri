package src.main.java.com.example.ch04.tsk10;

import java.util.logging.Logger;

/**
 * Use the MethodPrinter program in Section 4.5.1, “Enumerating Class
 * Members” (page 168) to enumerate all methods of the int[] class.
 * Extra credit if you can identify the one method (discussed in this chapter)
 * that is wrongly described.
 */

public class Program {
    private static final Logger logger = Logger.getLogger(src.main.java.com.example.ch07.tsk08.Program.class.getName());

    public static void main(String[] args) {
        //System.out.println(MethodPrinter.allMethods("src.main.java.com.example.src.main.java.com.example.ch02.tsk05.Point"));

        int[] hi = new int[10];

        logger.info(MethodPrinter.all(hi.getClass().getName()));

    }
}
