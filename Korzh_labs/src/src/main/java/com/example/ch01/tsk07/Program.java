package src.main.java.com.example.ch01.tsk07;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = (int)scanner.nextLong(),
            b = (int)scanner.nextLong();

        System.out.printf("Input: %d %d\n", a, b);

        int c = a + b;
        System.out.printf("a + b = %d\n", Integer.toUnsignedLong(c));

        c = a - b;
        System.out.printf("a - b = %d\n", Integer.toUnsignedLong(c));

        c = a / b;
        System.out.printf("a / b = %d\n", Integer.toUnsignedLong(c));

        c = a % b;
        System.out.printf("a %% b = %d\n", Integer.toUnsignedLong(c));

        c = a * b;
        System.out.printf("a * b = %d\n", Integer.toUnsignedLong(c));

    }
}