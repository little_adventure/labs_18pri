package src.main.java.com.example.ch05.tsk11;

import java.util.logging.Logger;

/**
 * Write a recursive factorial method in which you print all stack frames before
 * you return the value. Construct (but don't throw) an exception object of any kind
 * and get its stack trace, as described in Section 5.1.8, “Uncaught Exceptions and
 * the Stack Trace” (page 192).
 */

public class Program {
    private static final Logger logger = Logger.getLogger(src.main.java.com.example.ch07.tsk08.Program.class.getName());

    public static int factorial(int n) {
        new Throwable().printStackTrace();

        if (n == 1) return 1;
        return n * factorial(n - 1);
    }

    public static void main(String[] args) {
        logger.info(Integer.toString(factorial(5)));
    }
}
