package src.main.java.com.example.ch05.tsk12;

import java.util.Objects;
import java.util.logging.Logger;

/**
 * Compare the use of
 * Objects.requireNonNull(obj)
 * and
 * assert obj != null
 *
 * Give a compelling use for each
 */

public class Program {
    private static final Logger logger = Logger.getLogger(src.main.java.com.example.ch07.tsk08.Program.class.getName());

    public static void main(String[] args) {
        logger.info("hello");
        Object o1 = null;
        assert o1 != null: ("Object is null!");
        Object o2 = Objects.requireNonNull(o1);
    }
}
