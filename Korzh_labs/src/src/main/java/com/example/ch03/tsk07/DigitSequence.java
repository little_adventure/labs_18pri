package src.main.java.com.example.ch03.tsk07;

import java.util.Iterator;

public class DigitSequence implements Iterator<Integer> {
    private Integer i = 0;
    private final Integer total = 10;

    public DigitSequence() {}

    @Override
    public boolean hasNext() {
        return total > i;
    }

    @Override
    public Integer next() {
        return ++i;
    }

    @Override
    public void remove() {
        // I do nothing
    }

//    @Override
//    public void forEachRemaining(Consumer<? super Integer> action) {
//        Iterator.super.forEachRemaining(action);
//    }

}
