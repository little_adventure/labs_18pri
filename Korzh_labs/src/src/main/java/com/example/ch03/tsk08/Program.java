package src.main.java.com.example.ch03.tsk08;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Program {

    static class MyStringComparator implements Comparator<String> {
        @Override
        public int compare(String s, String to) {
            return s.length() - to.length();
        }
    }

    static boolean checkOrder(ArrayList<String> strings, Comparator<String> comp) {
        for (int i = 1; i < strings.size(); i++) {
            if (comp.compare(strings.get(i), strings.get(i - 1)) > 0)
                return false;
        }
        return true;
    }

    static void luckySort(ArrayList<String> strings, Comparator<String> comp) {

        while (!checkOrder(strings, comp)) {
            Collections.shuffle(strings);
        }
    }

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<String>() {{
            add("Hello");
            add("This");
            add("is");
            add("a");
            add("test");
            add("strings!");
        }};

        luckySort(list, new MyStringComparator());

        System.out.println(list);
    }
}
