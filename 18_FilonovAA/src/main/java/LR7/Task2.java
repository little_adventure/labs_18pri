package LR7;


import LR6.Task1;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

public class Task2 {
    private final static Logger log = LogManager.getLogger(Task2.class);

    public static void main(String[] args) {
        System.out.println("Task 7-2");
        ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList(
                "string 1",
                "some_other_string",
                "and oNe more"
        ));
        System.out.println("Original array: " + stringArrayList);
        System.out.println("toUpperViaIterator: " + toUpperViaIterator(stringArrayList));
        System.out.println("toUpperViaLoop: " + toUpperViaLoop(stringArrayList));
        System.out.println("toUpperViaReplace: " + toUpperViaReplace(stringArrayList));


    }

    public static ArrayList<String> toUpperViaIterator(ArrayList<String> strings) {
        ListIterator it = strings.listIterator();
        while (it.hasNext()) {
            it.set(it.next().toString().toUpperCase());
        }
        return strings;
    }

    public static ArrayList<String> toUpperViaLoop(ArrayList<String> strings) {
        ArrayList<String> copyOfStrings = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            String string = strings.get(i);
            copyOfStrings.add(string.toUpperCase());
        }
        return copyOfStrings;
    }

    public static ArrayList<String> toUpperViaReplace(ArrayList<String> strings) {
        strings.replaceAll(String::toUpperCase);
        return strings;
    }


}
