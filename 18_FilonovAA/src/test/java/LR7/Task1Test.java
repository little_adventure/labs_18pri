package LR7;

import org.junit.jupiter.api.Test;

import java.util.BitSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class Task1Test {

    @Test
    void test_allPrimeNumbersInHashSet() {
        Set<Integer> result = LR7.Task1.allPrimeNumbersInHashSet(100);
        assertEquals("[1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]", result.toString());
    }

    @Test
    void test_allPrimeNumbersInBitSet() {
        BitSet result = LR7.Task1.allPrimeNumbersInBitSet(100);
        assertEquals("{1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}", result.toString());
    }

}