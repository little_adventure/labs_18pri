package LR7;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Task2Test {

    @Test
    void test_toUpperViaIterator() {
        ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList(
                "string 1",
                "some_other_string",
                "and oNe more"
        ));

        ArrayList<String> result = LR7.Task2.toUpperViaIterator(stringArrayList);
        assertEquals(List.of("STRING 1", "SOME_OTHER_STRING", "AND ONE MORE"), result);
    }

    @Test
    void test_toUpperViaLoop() {
        ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList(
                "string 1",
                "some_other_string",
                "and oNe more"
        ));

        ArrayList<String> result = LR7.Task2.toUpperViaLoop(stringArrayList);
        assertEquals(List.of("STRING 1", "SOME_OTHER_STRING", "AND ONE MORE"), result);
    }

    @Test
    void test_toUpperViaReplace() {
        ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList(
                "string 1",
                "some_other_string",
                "and oNe more"
        ));

        ArrayList<String> result = LR7.Task2.toUpperViaReplace(stringArrayList);
        assertEquals(List.of("STRING 1", "SOME_OTHER_STRING", "AND ONE MORE"), result);
    }
}