package LR7;

import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class Task3Test {
    @Test
    void union() {
        Set<Integer> set1 = new HashSet<>(Set.of(1, 2, 3));
        Set<Integer> set2 = new HashSet<>(Set.of(2, 3, 4));
        Set<Integer> result  =LR7.Task3.union(set1, set2);
        assertEquals(Set.of(1, 2, 3,4),result);
    }

    @Test
    void intersection() {
        Set<Integer> set1 = new HashSet<>(Set.of(1, 2, 3));
        Set<Integer> set2 = new HashSet<>(Set.of(2, 3, 4));
        Set<Integer> result  =LR7.Task3.intersection(set1, set2);
        assertEquals(Set.of( 2, 3),result);
    }

    @Test
    void difference() {
        Set<Integer> set1 = new HashSet<>(Set.of(1, 2, 3));
        Set<Integer> set2 = new HashSet<>(Set.of(2, 3, 4));
        Set<Integer> result  =LR7.Task3.difference(set1, set2);
        assertEquals(Set.of(1,4),result);
    }
}